public class Launcher{
	public static void main(String[] args){
		//Object Instantiation
		Customer c1=new Customer(12,34,56);
		Customer c2=new Customer(1,3,5);
		Customer c3=new Customer(1,3,5);
		Product p1=new Product();
		base b=new Customer(1,3,5);
		try{
			//comparing an object of customer with null object reference
			System.out.println(ObjectComparer.compare(null,c2));
		}
		catch(NullPointerException e){
			System.out.println("Null object reference was passed "+e.getMessage());
		}
		catch(IllegalAccessException e){
			System.out.println("Inaccessible attribute "+e.getMessage());
		}
		catch(Exception ex){
			System.out.println("Something's not right."+ex.getMessage());
		}	

		try{
			//comparing an object of customer with another object reference of customer
			System.out.println(ObjectComparer.compare(c1,c2));
		}
		catch(NullPointerException npe){
			System.out.println("Null object reference was passed "+npe.getMessage());
		}
		catch(IllegalAccessException iae){
			System.out.println("Inaccessible attribute "+iae.getMessage());
		}
		catch(Exception exp){
			System.out.println("Something's not right."+exp.getMessage());
		}

		try{
			//comparing an object of customer with another object reference of customer
			System.out.println(ObjectComparer.compare(c3,c2));
		}
		catch(NullPointerException npe){
			System.out.println("Null object reference was passed "+npe.getMessage());
		}
		catch(IllegalAccessException iae){
			System.out.println("Inaccessible attribute "+iae.getMessage());
		}
		catch(Exception exp){
			System.out.println("Something's not right."+exp.getMessage());
		}

		try{
			//comparing an object of customer with object reference of Product class
			System.out.println(ObjectComparer.compare(b,c2));
		}
		catch(NullPointerException npe){
			System.out.println("Null object reference was passed "+npe.getMessage());
		}
		catch(IllegalAccessException iae){
			System.out.println("Inaccessible attribute "+iae.getMessage());
		}
		catch(Exception exp){
			System.out.println("Something's not right."+exp.getMessage());
		}
		
		try{
			//comparing an object of customer with object reference of Product class
			System.out.println(ObjectComparer.compare(p1,c2));
		}
		catch(NullPointerException npe){
			System.out.println("Null object reference was passed "+npe.getMessage());
		}
		catch(IllegalAccessException iae){
			System.out.println("Inaccessible attribute "+iae.getMessage());
		}
		catch(Exception exp){
			System.out.println("Something's not right."+exp.getMessage());
		}
	}
}