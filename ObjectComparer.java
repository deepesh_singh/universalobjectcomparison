import java.lang.reflect.Field;
import java.lang.Class;
public class ObjectComparer{
	/**	This method here compares any two objects of any Class
		and acts as an universal comparison class for a project.
	*/
	public static boolean compare(Object o,Object p) throws java.lang.IllegalAccessException,NullPointerException{
		boolean flag=false;
		//this condition checks if the classes are not of same class
		if(o.getClass()!=p.getClass()){
			flag=false;
		}
		//if they are equal 
		else if(o.getClass().equals(p.getClass() )){
			//we'll peep inside the class and retrieve all the declared fields of that class in an array 
			Field[] field=o.getClass().getDeclaredFields();
			flag=comparison(field,o,p);									//Calling the comparison method to compare the values of the fields
			if(flag){													//if the object's are having same state then we'll check for the attributes
				Class cls=o.getClass();									//inherited by child class
				cls=cls.getSuperclass();								//reading object's super class
				while(cls!=null){										//reading class variables until the Object class
					Field[] fi=cls.getDeclaredFields(); 				//rest is same
					flag=comparison(fi,o,p); 							//Calling the comparison method to compare the values of the fields
					if(!flag)
						break;
					cls=cls.getSuperclass();
				}
			}
		}
		if(flag){
			System.out.println("Same state");
		}
		else{
			System.out.println("Not in the Same state");
		}
		return flag;
	}

	private static boolean comparison(Field[] field,Object o,Object p) throws java.lang.IllegalAccessException,NullPointerException{
		/** This method here is to do the task of actual comparison of the values of the fields of the Objects in concern. */
		boolean flag=true;
		for(Field f:field){
			f.setAccessible(true);							//to access the private data members of the class
			if(f.get(o).equals(f.get(p))){
				flag=true;
			}
			else{
				flag=false;
				break;
			}
		}
		return flag;
	}
}